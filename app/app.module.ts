import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SliderComponent } from './slider/slider.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SectionComponent } from './section/section.component';
import { CartComponent } from './cart/cart.component';
import { ProduitComponent } from './produit/produit.component';
import { ProduitAddComponent } from './produit-add/produit-add.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';

import { AppRoutingModule } from './app-routing.module';

import { ProduitService } from './services/produit.service';
import { UserService } from './services/user.service';

@NgModule({
    imports: [ BrowserModule, FormsModule, HttpModule, AppRoutingModule ],
    declarations : [ 
        AppComponent, 
        HomeComponent, 
        NavbarComponent, 
        SliderComponent, 
        SidebarComponent, 
        SectionComponent, 
        CartComponent, 
        ProduitComponent,
        ProduitAddComponent,
        AdminComponent,
        LoginComponent
    ],
    providers: [ProduitService, UserService],
    bootstrap: [ AppComponent ]
})

export class AppModule{
    
}
