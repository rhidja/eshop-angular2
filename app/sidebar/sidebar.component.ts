import { Component, OnInit } from '@angular/core';

import { ProduitService } from '../services/produit.service';

@Component({
    selector: 'sidebar',
    templateUrl: 'templates/sidebar.component.html'
})

export class SidebarComponent implements OnInit{
    marques = [];

    types = [
        {nom: "Smartphone"},
        {nom: "Pc Portable"},
        {nom: "Pc Bureau"}
    ];

    selectedMarque = [];
    selectedType = [];

    constructor(private _produitService: ProduitService){}
    ngOnInit(){
        this._produitService.getMarques()
            .subscribe(resMarquesData => this.marques = resMarquesData);
    }

    onCheckedMarque(marque){
        //this._produitService.addCheckedMarque(marque);
    }

    onCheckedType(type){
        //this._produitService.addCheckedType(type);
    }
}