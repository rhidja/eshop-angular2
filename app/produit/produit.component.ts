import 'rxjs/add/operator/switchMap';
import { Component, OnInit }      from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location }               from '@angular/common';

import { Produit }  from '../models/produit';
import { ProduitService }  from '../services/produit.service';

@Component({
  moduleId: module.id,
  selector: 'produit-detail',
  templateUrl: '../../templates/produit.component.html'
})
export class ProduitComponent implements OnInit {
  produit: Produit;

  constructor(
    private _produitService: ProduitService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {

  }

  goBack(): void {
    this.location.back();
  }
}