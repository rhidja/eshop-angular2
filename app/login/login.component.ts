import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User }  from '../models/user';
import { UserService }  from '../services/user.service';

@Component({
    templateUrl: 'templates/login.component.html'
})
export class LoginComponent implements OnInit{
    
    constructor(private _userService: UserService, private _router: Router){}

    ngOnInit(){
        
    }

    loginSubmit(user){
        console.log(user);
        this._userService.login(user.mail, user.password).subscribe((result) => {
            if (result) {
                this._router.navigate(['']);
            }
        });
    }
}