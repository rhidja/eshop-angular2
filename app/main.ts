import { platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { AppModule} from './app.module';

// Compile And launch the modules
platformBrowserDynamic().bootstrapModule(AppModule);
