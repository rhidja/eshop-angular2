import { Component, OnInit } from '@angular/core';

import { ProduitService } from '../services/produit.service';
import { Produit } from '../models/produit';

@Component({
    selector: 'section',
    templateUrl: 'templates/section.component.html'
})

export class SectionComponent implements OnInit{
    produits : Produit[] = [];
    marques = [];

    types = [
        {nom: "Smartphone"},
        {nom: "PcPortable"},
        {nom: "PcBureau"}
    ];

    selectedMarque = [];
    selectedType = [];

    constructor(private _produitService: ProduitService){
        this.subscribeProduits();
    }
    
    ngOnInit(){
        this.subscribeMarques();
    }

    addToCart(produit: Produit){
        this._produitService.addToCart(produit);
        return false;
    }

    /**
     * Méthodes de gestion de filtres de produits.
     */
    onCheckedMarque(marque){
        if(this.selectedMarque.indexOf(marque) !== -1){
            this.selectedMarque.splice(this.selectedMarque.indexOf(marque), 1);
        }else{
            this.selectedMarque.push(marque);
        }
        
        this.subscribeProduits();
    }

    onCheckedType(type){
        if(this.selectedType.indexOf(type) !== -1){
            this.selectedType.splice(this.selectedType.indexOf(type), 1);
            return;
        }else{
            this.selectedType.push(type);
        }
        this.subscribeProduits();
    }

    subscribeProduits(){
        let filtres = { marques: this.selectedMarque, types: this.selectedType }
        this._produitService.getProduits(filtres)
            .subscribe(resProduitsData => this.produits = resProduitsData);
    }

    subscribeMarques(){
        this._produitService.getMarques()
            .subscribe(resMarquesData => this.marques = resMarquesData);    
    }
}