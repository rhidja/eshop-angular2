import { Component, OnInit } from '@angular/core';

import { ProduitService } from '../services/produit.service';
import { Produit } from '../models/produit';

@Component({
    templateUrl: 'templates/produit-add.component.html'
})
export class ProduitAddComponent implements OnInit{
    private res : any;
    
    constructor(private _produitService: ProduitService){}

    ngOnInit(){
        
    }

    addProduit(produit: Produit){
        this._produitService.addProduit(produit)
            .subscribe(resProduitsData => this.res = resProduitsData);
        console.log(this.res);
    }
}