import { Component, OnInit } from '@angular/core';

import { ProduitService } from '../services/produit.service';
import { Produit } from '../models/produit';

@Component({
    selector: 'cart',
    templateUrl: 'templates/cart.component.html'
})
export class CartComponent{

    cart : Produit[] = [];
    
    constructor(private _produitService: ProduitService){}

    ngOnInit(){
        this.cart = this._produitService.getCart();
    }

    removeFromCart(produit: Produit){
        this._produitService.removeFromCart(produit);
    }
        
}