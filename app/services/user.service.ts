import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

import { User } from '../models/user';

@Injectable()
export class UserService {
    private _urlServer : string = 'http://localhost:3000';
    private loggedIn = false;
    //private produits : Produit[] = [];
    //private cart : Produit[] = [];

    constructor(private _http: Http){
        this._http = _http;
        this._urlServer = 'http://localhost:3000';
        this.loggedIn = false;
        this.loggedIn = !!localStorage.getItem('auth_token');
    }

    login(email, password) {
        var _this = this;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http
            .post('/login', JSON.stringify({ email: email, password: password }), { headers: headers })
            .map(function (res) { return res.json(); })
            .map(function (res) {
            if (res.success) {
                localStorage.setItem('auth_token', res.auth_token);
                _this.loggedIn = true;
            }
            return res.success;
        });
    };
    register(user) {
    };
    logout = function () {
    };
}
