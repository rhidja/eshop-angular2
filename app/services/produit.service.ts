import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

import { Produit } from '../models/produit';

@Injectable()
export class ProduitService{
    private _urlServer : string = 'http://localhost:3000';
   
    private produits : Produit[] = [];
    private cart : Produit[] = [];

    constructor(private _http: Http){
        if(localStorage.getItem('cartItems') === null){
            localStorage.setItem('cartItems',JSON.stringify(this.cart));
        }
    }

    /**
     * Gestion des observables.
     */
    getProduits(filters){
        let params: URLSearchParams = new URLSearchParams();
        
        // Ajout du paramètre marques
        let marques = [];
        for(let marque of filters.marques){
             marques.push(marque.nom);
        }
        
        if( marques.length > 0) 
            params.set('marques', marques.toString());
        
        // Ajout du paramètre types
        let types = [];
        for(let type of filters.types){
             types.push(type.nom);
        }
        
        if( types.length > 0) 
            params.set('types', types.toString());

        return this._http.get(this._urlServer+'/produits', {search:params})
                   .map((response: Response) => response.json());
    }

    getMarques(){
        return this._http.get(this._urlServer+'/marques').map((response: Response) => response.json());
    }
    
    /**
     *  CRUD des produits.
     */
    
    addProduit(produit: Produit){
        let json = JSON.stringify(produit);
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this._http.post(this._urlServer+'/produits/add', json, headers);
    }

    /**
     * Méthodes de gestion du panier.
     */
    getCart(){   
        this.cart = JSON.parse(localStorage.getItem('cartItems'));
        return this.cart;
    }

    addToCart(produit : Produit){
        if(this.cart.indexOf(produit) !== -1){
            return;
        }

        this.cart.push(produit);
        localStorage.setItem('cartItems',JSON.stringify(this.cart));
    }

    removeFromCart(produit : Produit){
        this.cart.splice(this.cart.indexOf(produit), 1);
        localStorage.setItem('cartItems',JSON.stringify(this.cart));
    }
}