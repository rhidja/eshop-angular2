import { Component, OnInit } from '@angular/core';

import { ProduitService } from '../services/produit.service';
import { Produit } from '../models/produit';

@Component({
    selector: 'navbar',
    templateUrl: 'templates/navbar.component.html'
})

export class NavbarComponent implements OnInit{
    cart : Produit[] = [];
    
    constructor(private _produitService: ProduitService){}

    ngOnInit(){
        this.cart = this._produitService.getCart();
    }

    removeFromCart(produit: Produit){
        this._produitService.removeFromCart(produit);
    }
}