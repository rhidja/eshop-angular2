export class Produit {
    public _id: string;
    public nom: string;
    public marque: string;
    public type: string;
    public prix: number;
    public qtte: number;
    public description: string;
}