export class User {
    public _id: string;
    public nom: string;
    public prenom: string;
    public login: string;
    public password: string;
    public numTel: string;
    public email: string;
    public adresse: string;
}